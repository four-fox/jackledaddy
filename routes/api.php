<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::post('login', 'AuthController@login');
Route::post('register', 'AuthController@register');

Route::get('rated-artists', 'Api\SpotifyDataController@ratedArtists');
Route::get('get-artist-by-id', 'Api\SpotifyDataController@getArtistById');

Route::get('artist-top-tracks', 'Api\SpotifyDataController@artistTopTracks');



Route::group(['middleware' => 'auth:api'], function(){

	Route::get('artist', 'Api\SpotifyDataController@searchSpotify');
	Route::post('save-questionnaire', 'AuthController@save_questionnaire');
	Route::post('profie-save-update', 'AuthController@profieSaveUpdate');

	// Route::get('get-artist-by-id', 'Api\SpotifyDataController@getArtistById');

});
