<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserRating extends Model
{
    use HasFactory;

	protected $guarded = [];


    public function scopeAlbum()
    {
        return $query->where('type', 'album');
    }


    public function scopeArtist()
    {
        return $query->where('type', 'artist');
    }


    public function scopePlaylist()
    {
        return $query->where('type', 'playlist');
    }


    public function scopeTrack()
    {
        return $query->where('type', 'track');
    }


    

}
