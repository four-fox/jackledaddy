<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth; 
use Illuminate\Support\Facades\Hash;
use App\Models\User; 
use Validator;

class AuthController extends Controller
{
    /**
     * login api 
     *
     * @return \Illuminate\Http\Response
     */
    public function login()
    {
        $request = \Request::all();
        $validator = Validator::make($request, [ 
            'email'    => 'required|email', 
            'password' => 'required' 
        ]);

        // if vlaidtion fails
        if ($validator->fails()) { 
            return response()->json([
                'status'  => False,
                'data'    => $validator->errors(),
                'message' => 'Fields are required'
            ], 401);            
        }
        // parameters for login
        $request = ['email' => request('email'), 'password' => request('password')];

        // if auth success then create token
        if(Auth::attempt($request)){ 

            $user = Auth::user(); 
            $token['token_type'] =  'Bearer'; 
            $token['token'] =  $user->createToken('spotify')->accessToken; 
  
            $token['user_details'] =  $user; 
            return response()->json([
                'status'  => TRUE,
                'data'    => $token,
                'message' => 'Logged in successfully'
            ], 200); 

        }else{ 
            return response()->json([
                'status'  => False,
                'data'    => 'Unauthorised',
                'message' => 'error occurred'

            ], 401); 
        } 
    }

    /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [ 
            'name'         => 'required', 
            'email'        => 'required|unique:users,email', 
            'password'     => 'required', 
            // 'confirm_pass' => 'required|same:password', 
        ]);

        // if vlaidtion fails
        if ($validator->fails()) { 
            return response()->json([
                'status'  => False,
                'data'    => $validator->errors(),
                'message' => 'Logged in Fail'
            ], 401);            
        }

        // all user inputs
        $input = $request->all();
        $input['password'] = Hash::make($input['password']); 

        if (isset($input['confirm_pass'])) {
            unset($input['confirm_pass']);
        }
        // save user in table
        $user = User::create($input); 
        $success['token_type'] =  'Bearer'; 
        $success['token']   =  $user->createToken('Film')->accessToken;
        $success['user_details']    =  $user;

        return response()->json([
            'status'  => TRUE,
            'data'    => $success,
            'message' => 'Registration in successfully'
        ],200); 
    }



    /**
     * save questionare
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function save_questionnaire(Request $request)
    {
        try {

            $validator = Validator::make($request->all(), [ 
                'hiphip'       => 'required', 
                'album'        => 'required',
                'favorite_mc'  => 'required'
            ]);

            // if vlaidtion fails
            if ($validator->fails()) { 
                return response()->json([
                    'status'  => False,
                    'data'    => $validator->errors(),
                    'message' => 'Fields are required'
                ], 401);            
            }

            $user = User::find(\Auth::user()->id);
            $user->questionnaire_res = json_encode($request->all());
            $user->save();


            return response()->json([
                'status'  => TRUE,
                'data'    => $request->all(),
                'message' => 'questionnair added successfully'
            ], 200); 
            
        } catch (Exception $e) {

            return response()->json([
                'status'  => false ,
                'data'    => $e->getMessage(), 
                'message' => 'error occurred'
            ], 500);
            
        }
        
        //
    }


    public function profieSaveUpdate(Request $request)
    {
        try {

            $validator = Validator::make($request->all(), [ 
                'biography'        => 'required', 
                'interests'        => 'required'
            ]);

            // return json_encode($request->interests);



            // if vlaidtion fails
            if ($validator->fails()) { 
                return response()->json([
                    'status'  => False,
                    'data'    => $validator->errors(),
                    'message' => 'Fields are required'
                ], 401);            
            }

            $user = User::find(\Auth::user()->id);
            if (empty($user)) {
                $user = new User;
            }
            
            $user->biography = $request->biography;
            $user->interests = json_encode($request->interests);
            $user->save();

            return response()->json([
                'status'  => true,
                'data'    => $request->all(), 
                'message' => 'updated successfully'
            ], 500);

            
        } catch (Exception $e) {
            return response()->json([
                'status'  => false ,
                'data'    => $e->getMessage(), 
                'message' => 'error occurred'
            ], 500);
        }
    }

    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        return response()->json([
            'status'  => false ,
            'data'    => $e->getMessage(), 
            'message' => 'error occurred'
        ]);
    }
}