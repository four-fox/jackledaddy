<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\SpotifyUsers;
use Spotify;
use DB;
use Validator;
use App\Http\Traits\HelperFunction;


class SpotifyDataController extends Controller
{
    use HelperFunction;

    public $limit = 50; 
    private const SPOTIFY_API_TOKEN_URL = 'https://accounts.spotify.com/api/token';

    /**
     * Display a listing of the searchSpotify.
     *
     * @return \Illuminate\Http\Response
     */
    public function searchSpotify(Request $request)
    {
        $requests = $request->all();
        $validator = Validator::make($requests, [ 
            'search' => 'required', 
            'type'   => 'required'
        ]);

        // if vlaidtion fails
        if ($validator->fails()) { 
            return response()->json([
                'status'  => false,
                'data'    => $validator->errors(),
                'message' => 'error occurred'
            ], 422);            
        }

        try {

            $offset = $request->offset ? $request->offse : 0;
            $search = $request->search ? $request->search : 'Muse';
            $type   = $request->type ? $request->type : 'artist';

            $data = Spotify::searchItems($search, $type)->limit($this->limit)->offset($offset)->get();
            // $data = Spotify::searchArtists($search)->limit($this->limit)->offset($offset)->get();

            return response()->json([
                'status'  => true ,
                'data'    => $data , 
                'message' => 'data loaded'
            ], 200);
            
        } catch (Exception $e) {

            return response()->json([
                'status'  => false ,
                'data'    => $e->getMessage(), 
                'message' => 'error occurred'
            ], 500);
            
        }
        


    }

    /**
     * Show rated Artists
     *
     * @return \Illuminate\Http\Response
     */
    public function ratedArtists(Request $request)
    {
        try {

            $sUsers = SpotifyUsers::withCount(['spotifyRatings as average_rating' => function($query) {
                return $query->select(DB::raw('coalesce(avg(rate_num),0)'));
            }]);

            if (!empty($request->limit)) {
                 $sUsers->take($request->limit);
            }
  
           

            $sUsers = $sUsers->orderBy('average_rating','desc')->get();

            $sUsers->makeHidden('spotify_user_json');

            // dd($this->test());

               return $artistAlbums = Spotify::artistAlbums('0TnOYISbd1XYRBk9myaSseg')->get();

            // foreach ($sUsers as $key => $value) {
            //     $value->total_albums = 1;
            // }

            return response()->json([
                'status'  => true ,
                'data'    => $sUsers , 
                'message' => 'data loaded'
            ], 200);

            
        } catch (Exception $e) {

            return response()->json([
                'status'  => false ,
                'data'    => $e->getMessage(), 
                'message' => 'error occurred'
            ], 500);
            
        }
    }

    /**
     * get artist by artist id (spotify).
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getArtistById(Request $request)
    {

        $requests = $request->all();
        $validator = Validator::make($requests, [ 
            'artist_id' => 'required'
        ]);

        // if vlaidtion fails
        if ($validator->fails()) { 
            return response()->json([
                'status'  => false,
                'data'    => $validator->errors(),
                'message' => 'error occurred'

            ], 422);            
        }

        try {

            $data = Spotify::artist($request->artist_id)->get();
            return response()->json([
                'status'  => true ,
                'data'    => $data , 
                'message' => 'data loaded'
            ], 200);
            
        } catch (Exception $e) {

            return response()->json([
                'status'  => false ,
                'data'    => $e->getMessage(), 
                'message' => 'error occurred'
            ], 500);
            
        }

        //
    }

    /**
     * Get the artist's top tracks by ID.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function artistTopTracks(Request $request)
    {
        $requests = $request->all();
        $validator = Validator::make($requests, [ 
            'artist_id' => 'required'
        ]);

        // if vlaidtion fails
        if ($validator->fails()) { 
            return response()->json([
                'status'  => false,
                'data'    => $validator->errors(),
                'message' => 'error occurred'

            ], 422);            
        }

        try {


            // return $request->artist_id;

            $data = Spotify::artistTopTracks($request->artist_id)->country('US')->get();
            return response()->json([
                'status'  => true ,
                'data'    => $data , 
                'message' => 'data loaded'
            ], 200);

            
        } catch (Exception $e) {

             return response()->json([
                'status'  => false ,
                'data'    => $e->getMessage(), 
                'message' => 'error occurred'
            ], 500);
            
        }
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
